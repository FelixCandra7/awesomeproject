import React, {Component} from 'react';
import { AppRegistry, TextInput,
         Text, View, StyleSheet, Alert, TouchableOpacity,
         Image, ScrollView } from 'react-native';

export default class Login extends Component{
  
  state = {
    username: '',
    password: '',
  }
  
  onPress = () => {
    Alert.alert("Username: "+this.state.username+"\nPassword: "+this.state.password)
  }


  render(){
    let pic= { 
      uri: 'https://imgur.com/9wzLNfD.png'};

    return(
      <ScrollView>
      <View style={styles.container}>

        <View style={styles.header}>
          <Blink text="Free Orange"/>
        </View>

        <View style={styles.iconContainer}>
          <Image source={pic} style={styles.appIcon}/>
        </View>

        <View style={styles.textContainer}> 

          <TextInput
          style={styles.textbox}
          placeholder="Username"
          onChangeText={(username) => this.setState({username})}
          />

          <TextInput
          style={styles.textbox}
          placeholder="Password"
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          />

        </View>

        <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={this.onPress}>
            <View style={styles.loginButton}>
              <Text style={styles.buttonText}>Login</Text>
            </View>
        </TouchableOpacity>
        </View>

      </View>
      </ScrollView>
    );
  }
}

class Blink extends Component{
  constructor(props){
    super(props);
    this.state = {isShowingText: true};
    // Toggle the state every second
    setInterval(() => (
    this.setState(previousState => (
      { isShowingText: !previousState.isShowingText }
    ))
    ), 1000);
  }

  render(){
    if(!this.state.isShowingText){
      return null;
    }

    return(<Text style={styles.title}>{this.props.text}</Text>);  
  }
}


const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white',
  },
  
  header:{
    height: 75,
    flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#e62e00",
  },

  title:{
    fontSize: 40, 
    fontWeight: 'bold', 
    color: 'white',
  },

  iconContainer:{
    height: 210,
    paddingLeft: 100,
    justifyContent: 'center'
  },

  appIcon:{
    height: 189,
    width: 205,
  },

  textContainer:{
    paddingTop: 10,
    height: 140,
    alignItems: 'center',
  },

  textbox:{
    width: 300,
    height: 40,
    backgroundColor: '#e6e6e6',
    margin: 10,
  },

  buttonContainer:{
    height: 80,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  loginButton:{
    margin: 15,
    width: 150,
    alignItems: 'center',
    backgroundColor: 'black'
  },

  buttonText:{
    padding: 5,
    color: 'orange',
    fontSize: 30,
  },
});
